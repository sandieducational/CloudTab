CloudTab
========

This is Chrome Extension designed to replace the new tab UI.
It uses JQuery to animate the bookmarks ,apps and custom tags.

This extension is designed and developed by Sandeep Jagtap and Ishan Trikha.
